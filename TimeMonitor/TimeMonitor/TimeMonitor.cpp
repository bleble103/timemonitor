﻿// TimeMonitor.cpp : Definiuje punkt wejścia dla aplikacji.
//


#include <iostream>
#include "httplib.h"
#include "sqlite3.h"
#include<ctime>
#include<iomanip>
#include<sstream>
#include<process.h>

#include "framework.h"
#include "TimeMonitor.h"


using namespace httplib;

sqlite3* db;
Response* globalResponse;
bool dbAvailable = false;
std::string globalStringResponse;

std::string getCurrentDate() {
    //2019-01-01
    static std::string staticDate = "";
    if (staticDate == "") {
        auto t = std::time(nullptr);
        auto tm = *std::localtime(&t);
        std::stringstream ss;
        ss << std::put_time(&tm, "%Y-%m-%d");
        staticDate = ss.str();
    }
    
    return staticDate;
}

std::string getCurrentTime() {
    //2019-01-01
    static std::string staticDate = "";
    if (staticDate == "") {
        auto t = std::time(nullptr);
        auto tm = *std::localtime(&t);
        std::stringstream ss;
        ss << std::put_time(&tm, "%R");
        staticDate = ss.str();
    }

    return staticDate;
}

void signalToDatabase() {

    while (!dbAvailable);
    dbAvailable = false;

    std::cout << "Signal" << std::endl;
    std::string sqlUpdate = "UPDATE czasy SET minuty=minuty+1 WHERE data='" + getCurrentDate() + "';";
    char* errMsg;
    if (sqlite3_exec(db, sqlUpdate.c_str(), NULL, NULL, &errMsg) != SQLITE_OK) {
        std::cout << "DB update error" << std::endl;
        sqlite3_free(errMsg);
    }

    dbAvailable = true;
}

void signalActivity(void* param) {
    while (true) {
        Sleep(60000);
        signalToDatabase();
    }
}

std::string yearMonth = "";
int minutes = 0;

std::string intToString(int a) {
    std::stringstream ss;
    ss << a;
    return ss.str();
}

int stringToInt(std::string str) {
    std::istringstream iss(str);
    int i;
    iss >> i;
    return i;
}

std::string lastDateToAppend;

void appendHours(std::string date) {
    int hours = minutes / 60;
    minutes = minutes % 60;

    std::string godzinString = "godzin";
    if (hours == 1) godzinString = "godzina";
    if (hours >= 2 && hours <= 4) godzinString = "godziny";
    if (hours > 20 && (hours % 10 >= 2 && hours % 10 <= 4)) godzinString = "godziny";

    std::string minutString = "minut";
    if (minutes == 1) minutString = "minuta";
    if (minutes >= 2 && minutes <= 4) minutString = "minuty";
    if (minutes > 20 && (minutes % 10 >= 2 && minutes % 10 <= 4)) minutString = "minuty";

    globalStringResponse += "<h2>" + yearMonth + ": " + intToString(hours) + " " + godzinString + ", " + intToString(minutes) + " "+ minutString +"</h2><br>";
    minutes = 0;
    yearMonth = std::string(date).substr(0, 7);
}

static int callback(void* data, int argc, char** argv, char** azColName) {
    int i;
    std::string buildResponse;
    std::stringstream ss;
    fprintf(stderr, "%s: ", (const char*)data);

    for (i = 0; i < argc; i++) {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        //ss << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << ";";
        if (std::string(azColName[i]) == "data") {
            lastDateToAppend = std::string(argv[i]);
            if (yearMonth == "") {
                //first date
                yearMonth = std::string(argv[i]).substr(0, 7);
            }
            else {
                if (yearMonth != std::string(argv[i]).substr(0, 7)) {
                    appendHours(std::string(argv[i]));
                }
            }
            ss << "<b>"<< argv[i] << "</b>: ";
        }
        else if (std::string(azColName[i]) == "minuty") {
            int thatMinutes = stringToInt(std::string(argv[i]));
            minutes += thatMinutes;
            int thatHours = thatMinutes / 60;
            thatMinutes = thatMinutes % 60;

            std::string godzinString = "godzin";
            if (thatHours == 1) godzinString = "godzina";
            if (thatHours >= 2 && thatHours <= 4) godzinString = "godziny";
            if (thatHours > 20 && (thatHours % 10 >= 2 && thatHours % 10 <= 4)) godzinString = "godziny";

            std::string minutString = "minut";
            if (thatMinutes == 1) minutString = "minuta";
            if (thatMinutes >= 2 && thatMinutes <= 4) minutString = "minuty";
            if (thatMinutes > 20 && (thatMinutes % 10 >= 2 && thatMinutes % 10 <= 4)) minutString = "minuty";


            ss << thatHours << " "<<godzinString<<", "<<thatMinutes<<" "<< minutString;
        }
        else if (std::string(azColName[i]) == "poczatek" && argv[i] != NULL) {
            std::string poczatek = std::string(argv[i]);
            ss << " od: " << poczatek;
        }
    }
    ss << " <br>";

    printf("\n");

    //std::string buildResponse = "<h1>Naglowek</h1> <h2>Kolejny</h2>";
    //buildResponse = ss.str();
    globalStringResponse += ss.str() + "\n";

    return 0;
}



int initializeTimeMonitor()
{


    char* zErrMsg = 0;
    int rc;

    rc = sqlite3_open("czasy.db", &db);

    if (rc) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        return(0);
    }
    else {
        fprintf(stderr, "Opened database successfully\n");
    }
    //sqlite3_close(db);

    _beginthread(signalActivity, 0, NULL);

    //Init the day
    std::string thisDay = getCurrentDate();
    std::string thisTime = getCurrentTime();
    std::string initSql = "INSERT INTO czasy (data, minuty, poczatek)\
        SELECT '" + thisDay + "', 0, '" + thisTime + "'\
        WHERE NOT EXISTS(SELECT 1 FROM czasy WHERE data = '" + thisDay + "'); ";
    char* errMsg;
    if (sqlite3_exec(db, initSql.c_str(), NULL, NULL, &errMsg) != SQLITE_OK) {
        std::cout << "DB Init day error" << std::endl;
        return 1;
    }

    dbAvailable = true;

    Server svr;

    svr.Get("/godziny", [](const Request& req, Response& res) {
        while (!dbAvailable);
        dbAvailable = false;
        globalStringResponse = "";

        /* Create SQL statement */
        std::string sql;
        sql = "SELECT * from czasy ORDER BY data ASC;";

        char* zErrMsg = 0;
        int rc;
        const char* data = "select all query";

        /* Execute SQL statement */
        globalResponse = &res;
        rc = sqlite3_exec(db, sql.c_str(), callback, (void*)data, &zErrMsg);

        if (rc != SQLITE_OK) {
            fprintf(stderr, "SQL error: %s\n", zErrMsg);
            sqlite3_free(zErrMsg);
            res.set_content("Error", "text/html");
        }
        else {
            fprintf(stdout, "Operation done successfully\n");
            appendHours(lastDateToAppend);
            yearMonth = "";
            minutes = 0;
            res.set_content(globalStringResponse, "text/html");
        }

        dbAvailable = true;


        //std::string buildResponse = "<h1>Naglowek</h1> <h2>Kolejny</h2>";
        //res.set_content(buildResponse, "text/html");
        });

    svr.Get(R"(/numbers/(\d+))", [&](const Request& req, Response& res) {
        auto numbers = req.matches[1];
        res.set_content(numbers, "text/plain");
        });

    svr.Get("/stop", [&](const Request& req, Response& res) {
        svr.stop();
        });

    svr.listen("0.0.0.0", 2137);

    exit(0);
}

#define MAX_LOADSTRING 100

// Zmienne globalne:
HINSTANCE hInst;                                // bieżące wystąpienie
WCHAR szTitle[MAX_LOADSTRING];                  // Tekst paska tytułu
WCHAR szWindowClass[MAX_LOADSTRING];            // nazwa klasy okna głównego

// Przekaż dalej deklaracje funkcji dołączone w tym module kodu:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: W tym miejscu umieść kod.

    initializeTimeMonitor();
    // Inicjuj ciągi globalne
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_TIMEMONITOR, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Wykonaj inicjowanie aplikacji:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TIMEMONITOR));

    MSG msg;

    // Główna pętla komunikatów:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNKCJA: MyRegisterClass()
//
//  PRZEZNACZENIE: Rejestruje klasę okna.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TIMEMONITOR));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_TIMEMONITOR);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNKCJA: InitInstance(HINSTANCE, int)
//
//   PRZEZNACZENIE: Zapisuje dojście wystąpienia i tworzy okno główne
//
//   KOMENTARZE:
//
//        W tej funkcji dojście wystąpienia jest zapisywane w zmiennej globalnej i
//        jest tworzone i wyświetlane okno główne programu.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Przechowuj dojście wystąpienia w naszej zmiennej globalnej

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   //ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNKCJA: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PRZEZNACZENIE: Przetwarza komunikaty dla okna głównego.
//
//  WM_COMMAND  - przetwarzaj menu aplikacji
//  WM_PAINT    - Maluj okno główne
//  WM_DESTROY  - opublikuj komunikat o wyjściu i wróć
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Analizuj zaznaczenia menu:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Tutaj dodaj kod rysujący używający elementu hdc...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Procedura obsługi komunikatów dla okna informacji o programie.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
